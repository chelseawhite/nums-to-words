//This question was particularly helpful for me figuring out hw to determine place value:
//https://stackoverflow.com/questions/24226324/getting-place-values-of-a-number-w-modulus

//Initialize arrays with the different values I'll utilize

//oneNumbers will be used if i is a single digit number, if i is a two digit number whose first number is not 1, or if i is a three
//digit number whose second number is not 1. This will also be used to add to the front of "hundred" when there are three digits involved.
const oneNumbers = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];

//teenNumbers will be used if i is a two digit number whose first number is 1, or if i is a three digit number whose second digit is 1
const teenNumbers = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];

//tenNumbers will be used if i is a two digit number whose first number is greater than 1, or if i is a three digit number whose second
//digit is greater than 1
const tenNumbers = ["", "", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"];

//Initializes new array holder for all numbers
const allNumbers =[]

//Function to print numbers to words in an array 1-20
function numbersToWords() {
    
    for (let i=1; i<=1000; i++) {
        
        //get the oneNumbers value of all numbers less than 10, push that value to the allNumbers array
        if (i < 10) {
            let ones = Math.floor(i % 10);
            ones = " " + oneNumbers[ones];
            allNumbers.push(ones);
        }

        //get the teenNumbers value of all numbers 10-19, push that value to the allNumbers array
        else if (i >= 10 && i < 20) {
            let teens = i % 10;
            teens = " " + teenNumbers[teens];
            allNumbers.push(teens);
        }

        //get the tenNumbers value of all numbers 20-99, and if necessary concatenate appropriate oneNumbers of all numbers and
        // push product to the allNumbers array
        else if (i>=20 && i < 100) {
            if (i % 10 === 0) {
                let tens = Math.floor(i/10%10);
                tens = " " + tenNumbers[tens];
                allNumbers.push(tens);
            }
            else if (i % 10 !== 0) {
                let ones = Math.floor(i%10);
                ones = oneNumbers[ones];
                let tens = Math.floor(i/10%10);
                tens = tenNumbers[tens];
                let combinedNumbers = " " + tens + " " + ones;
                allNumbers.push(combinedNumbers);
            }
        }

        //get the oneNumbers value of all numbers + hundred and determine which other method to use to move forward.
        else if (i>=100 && i<1000){

            //Determine the tens place for all hundreds with 0 as an ending
            if (i/10%10 > 1) {
                //With 0 as an ending
                if (i%10 === 0) {
                    let tens = Math.floor(i/10%10);
                    tens = tenNumbers[tens];
                    let hundreds = Math.floor(i/100%10);
                    hundreds = oneNumbers[hundreds] + " hundred ";
                    let combinedNumbers = " " + hundreds + tens;
                    allNumbers.push(combinedNumbers);
                }
                //Without 0 as an ending
                else if (i%10 >=1){
                    let ones = Math.floor(i%10);
                    ones = oneNumbers[ones];
                    let tens = Math.floor(i/10%10);
                    tens = tenNumbers[tens];
                    let hundreds = Math.floor(i/100%10);
                    hundreds = oneNumbers[hundreds] + " hundred ";
                    let combinedNumbers = " " + hundreds + tens + " " + ones;
                    allNumbers.push(combinedNumbers);
                }
            }

            //Determine plain ole hundreds no fancy stuff
            else if (i % 10 === 0) {
                let hundreds = Math.floor(i/100%10);
                hundreds = oneNumbers[hundreds] + " hundred ";
                allNumbers.push(hundreds);
            }

            //Determine hundreds that don't end in 0 and don't have a 10's value
            else if (i % 10 !== 0 && i/10%10 === 0) {
                    let ones = Math.floor(i%10);
                    ones = oneNumbers[ones];
                    let hundreds = Math.floor(i/100%10);
                    hundreds = oneNumbers[hundreds] + " hundred";
                    combinedNumbers = " " + hundreds + ones;
                    allNumbers.push(combinedNumbers);
                }

            //Determine hundreds in the teens
            else if (i/10%10 === 1) {
                let teens = Math.floor(i/10%10);
                teens = teenNumbers[teens];
                let hundreds = Math.floor(i/100%10);
                hundreds = oneNumbers[hundreds] + " hundred ";
                combinedNumbers = " " + hundreds + teens;
                allNumbers.push(combinedNumbers);
            }
        }
           
        //Determine one thousand - Elle Woods voice "We did it!"
        else if (i === 1000) {
            let thousand = Math.floor(i/1000%10);
            thousand = oneNumbers[thousand] + " thousand";
            allNumbers.push(thousand);
        }
    }
    
}
numbersToWords();
console.log(allNumbers);


let newDiv = document.createElement('div');
newDiv.innerHTML = allNumbers
let destination = document.getElementById('numbersInWords');
destination.appendChild(newDiv);


